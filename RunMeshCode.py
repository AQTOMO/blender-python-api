# -*- coding: utf-8 -*-
import sys
import subprocess

argvs = sys.argv  # コマンドライン引数を格納したリストの取得
argc = len(argvs) # 引数の個数

def ply2off(obj):
    cmd1 = "perl ../ply2off.pl "
    cmd2 = ".ply > "
    cmd3 = ".off"
    cmd = cmd1 + obj + cmd2 + obj + cmd3

    subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    print cmd


def off_to_xyzn(obj):
    cmd1 = "python ../off_to_xyzn.py "
    cmd2 = ".off "
    cmd3 = ".xyzn"
    cmd = cmd1 + obj + cmd2 + obj + cmd3

    subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    print cmd

def makeoff(cmd):
    subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)


def get_lines(cmd):

    #:param cmd: str 実行するコマンド.
    #:rtype: generator
    #:return: 標準出力 (行毎).

    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    while True:
        line = proc.stdout.readline()
        if line:
            yield line

        if not line and proc.poll() is not None:
            break



if __name__ == '__main__':
    #subprocess.call(["pwd"])
    #ply2off(obj = 'double-torus_selected')
    #off_to_xyzn(obj = 'double-torus_selected')

    if argvs[1] == 'cylinderLM':
        objname = 'Selected_Objects/' + argvs[2]
        progname = 'RunFitCylinderLM '
        s3 = argvs[2] + '_CylinderLM.off'
        meshname = 'cylinder'
        print 'cylinderLM output'

    elif argvs[1] == 'cylinderQ':
        objname = 'Selected_Objects/' + argvs[2]
        progname = 'RunFitCylinderQ '
        s3 = argvs[2] + '_CylinderQ.off'
        meshname = 'quadric'
        print 'cylinderQ output'

    elif argvs[1] == 'cone':
        objname = 'Selected_Objects/' + argvs[2]
        progname = 'RunFitConeQ '
        s3 = argvs[2] + '_Cone.off'
        meshname = 'quadric'
        print 'cone output'

    elif argvs[1] == 'ellipsoid':
        objname = 'Selected_Objects/' + argvs[2]
        progname = 'RunFitEllipsoid '
        s3 = argvs[2] + '_Ellipsoid.off'
        meshname = 'quadric'
        print 'ellipsoid output'

    elif argvs[1] == 'planeQ':
        objname = 'Selected_Objects/' + argvs[2]
        progname = 'RunFitPlaneQ '
        s3 = argvs[2] + '_PlaneQ.off'
        meshname = 'quadric'
        print 'planeQ output'

    elif argvs[1] == 'sphereQ':
        objname = 'Selected_Objects/' + argvs[2]
        progname = 'RunFitSphereQ '
        s3 = argvs[2] + '_SphereQ.off'
        meshname = 'sphere'
        print 'sphereQ output'

    elif argvs[1] == 'torusQ':
        objname = 'Selected_Objects/' + argvs[2]
        progname = 'RunFitTorusQ '
        s3 = argvs[2] + '_TorusQ.off'
        meshname = 'torus'
        print 'torusQ output'


    s1 = ''
    s2 = ''

    for bbox in get_lines(cmd='python ../bbox.py ' + objname + '.xyzn'):
        sys.stdout.write(bbox)
        s1 += bbox
    for RunFit in get_lines(cmd='./../demo/' + progname + objname + '.xyzn'):
        sys.stdout.write(RunFit)
        s2 += RunFit

    s = './../Mesh_MC/mesh_' + meshname + ' '
    s1 = s1.replace("\n", " ")
    s2 = s2.replace("\n", " ")
    s = s + s1 + s2 + s3

    print(s)
    makeoff(cmd=s)
