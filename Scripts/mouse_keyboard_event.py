import bpy

bl_info = {
    "name" : "Mouse/Keyboard event",
    "author" : "Tomohiro",
    "version" : (0,1),
    "blender" : (2, 74, 0),
    "location" : "UV > Mouse/Keyboard event",
    "description" : "Mouse/Keyboard event.",
    "warning" : "",
    "support" : "TESTING",
    "wiki_url" : "",
    "tracker_url" : "",
    "category" : "UV"
}


running = False     # イベントの取得中であればTrue

class MouseKeyboardEvent(bpy.types.Operator):
    """Mouse/Keyboard event."""

    bl_idname = "uv.mouse_keyboard_event"
    bl_label = "Mouse/Keyboard event"
    bl_description = "Mouse/Keyboard event"
    bl_options = {'REGISTER', 'UNDO'}

    obj_scale = 1.0     # オブジェクトの拡大率
    orig_x = 0.0        # ボタンを押したときのX座標
    orig_scale = []     # ボタンを押したときのオブジェクトの拡大率

    def modal(self, context, event):
        global running

        # 動作していない場合は終了
        if running is False:
            return {'PASS_THROUGH'}

        # オブジェクトへ拡大率を適用
        active_obj = bpy.context.active_object
        active_obj.scale[0] = self.orig_scale[0] * self.obj_scale
        active_obj.scale[1] = self.orig_scale[1] * self.obj_scale
        active_obj.scale[2] = self.orig_scale[2] * self.obj_scale

        # マウス/キーボードのイベントを扱う処理

        # マウス移動 - マウスのX座標をもとにオブジェクトの拡大率を設定
        if event.type == 'MOUSEMOVE':
            factor = 1.0 / 100.0
            self.obj_scale = 1.0 + (event.mouse_x - self.orig_x) * factor
            return {'RUNNING_MODAL'}
        # スペースキーを押した時 - 拡大率をボタンを押した時に戻す
        elif event.type == 'SPACE':
            if event.value == 'PRESS':
                self.orig_x = event.mouse_x
                return {'RUNNING_MODAL'}
        # 右クリックを押した時 - 変更した拡大率を確定して操作を終了
        if event.type == 'RIGHTMOUSE':
            if event.value == 'PRESS':
                running = False
                return {'FINISHED'}

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        global running

        if context.area.type == 'VIEW_3D':
            # 動作していない状態でボタンを押されたら、各イベントを受け付ける
            if running is False:
                running = True
                # 初期状態の設定
                self.obj_scale = 1.0
                self.orig_x = event.mouse_x
                active_obj = bpy.context.active_object
                self.orig_scale = [
                    active_obj.scale[0],
                    active_obj.scale[1],
                    active_obj.scale[2]
                ]
                # modalハンドラの設定
                context.window_manager.modal_handler_add(self)
                return {'RUNNING_MODAL'}
        return {'CANCELLED'}

# 'N'キーを押したときにVIEW 3Dの右側に表示されるメニューにボタンを設置
class OBJECT_PT_MKET(bpy.types.Panel):
    bl_label = "Mouse/Keyboard Event"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"

    def draw(self, context):
        sc = context.scene
        layout = self.layout
        # ボタンの配置
        layout.operator(MouseKeyboardEvent.bl_idname, text="Start", icon="PLAY")


def register():
    bpy.utils.register_module(__name__)


def unregister():
    bpy.utils.unregister_module(__name__)


if __name__ == "__main__":
    register()
